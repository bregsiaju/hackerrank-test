"use strict";

const fs = require("fs");

process.stdin.resume();
process.stdin.setEncoding("utf-8");

let inputString = "";
let currentLine = 0;

process.stdin.on("data", function (inputStdin) {
  inputString += inputStdin;
});

process.stdin.on("end", function () {
  inputString = inputString.split("\n");

  main();
});

function readLine() {
  return inputString[currentLine++];
}

/*
 * Complete the 'camelcase' function below.
 *
 * The function is expected to return an INTEGER.
 * The function accepts STRING s as parameter.
 */

function camelcase(s) {
  // Write your code here

  // set count to 1 bcz the first word write in lowercase
  let count = 1;

  // get the unicode for every character using charCodeAt()
  for (let i = 1; i < s.length - 1; i++) {
    // compare with the unicode of A-Z (uppercase)
    if (s[i].charCodeAt(0) >= 65 && s[i].charCodeAt(0) <= 90) {
      count++;
    }
  }
  return count;
}

// console.log(camelcase("cintaTerlarang")); // expect output 2

function main() {
  const ws = fs.createWriteStream(process.env.OUTPUT_PATH);

  const s = readLine();

  const result = camelcase(s);

  ws.write(result + "\n");

  ws.end();
}
