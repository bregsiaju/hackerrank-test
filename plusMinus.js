'use strict';

process.stdin.resume();
process.stdin.setEncoding('utf-8');

let inputString = '';
let currentLine = 0;

process.stdin.on('data', function (inputStdin) {
  inputString += inputStdin;
});

process.stdin.on('end', function () {
  inputString = inputString.split('\n');

  main();
});

function readLine() {
  return inputString[currentLine++];
}

/*
 * Complete the 'plusMinus' function below.
 *
 * The function accepts INTEGER_ARRAY arr as parameter.
 */

function plusMinus(arr) {
  // Write your code here
  const n = arr.length;
  // console.log(arr);

  let positive = 0;
  let negative = 0;
  let zeros = 0;

  for (let i = 0; i < n; i++) {
    if (arr[i] > 0) {
      positive++;
    } else if (arr[i] < 0) {
      negative++;
    } else {
      zeros++;
    }
  }

  // hitung perbandingan jumlah masing2 jenis angka
  let proportionPositive = positive / n;
  let proportionNegative = negative / n;
  let proportionZeros = zeros / n;

  // menambahkan 6 angka di belakang koma
  console.log(proportionPositive.toFixed(6));
  console.log(proportionNegative.toFixed(6));
  console.log(proportionZeros.toFixed(6));

  return 0;
}

// sample
// const test = plusMinus([-4, 3, -9, 0, 4, 10]);
/* expected output
0.500000
0.333333
0.166667 */
// console.log(test);

function main() {
  const n = parseInt(readLine().trim(), 10);

  const arr = readLine()
    .replace(/\s+$/g, '')
    .split(' ')
    .map((arrTemp) => parseInt(arrTemp, 10));

  plusMinus(arr);
}
